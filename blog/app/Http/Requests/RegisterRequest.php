<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'required|email',
            'password'=>'required|confirmed|regex:/^(?=.{8,45})(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(?=.[!@#\$%\^&])$/',
        ];
    }
    public function messages()
    {
        return [
            'required'=>':attribute Không được để trống',
            'same'=>':attribute Nhập lại password',
            'email'=>':attribute Phải là email',
            'regex'=>':attribute Password phải gồm ít nhất 1 ký tự thường, 1 ký tự in hoa, 1 số và 1 ký tự đặc biệt',
        ];
    }
    public function store(RegisterRequest $request){
        return $request->all();
    }
}

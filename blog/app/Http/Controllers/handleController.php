<?php
namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use Illuminate\Http\Request;

class handleController extends Controller
{
    public function getForm(){
        return view('form');
    }
    public function handleRequest(Request $request){
        return $request->all();
    }
    public function store(RegisterRequest $request)
    {
        return $request->all();
    }

}
